
# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

### Fixed

- [#3392188](https://www.drupal.org/project/image_field_to_media/issues/3392188) - Lacking changelog and release notes.

## [2.0.2] - 2023-10-5

### Fixed

- [#3391287](https://www.drupal.org/project/image_field_to_media/issues/3391287) - Duplicate media for non-existing
source image files.
- [#3391960](https://www.drupal.org/project/image_field_to_media/issues/3391960) - Pictures of the created media field
are rendered in a different style than pictures of the original image field.
- [#3274349](https://www.drupal.org/project/image_field_to_media/issues/3274349) - Field field_media_image is unknown.
in Drupal\Core\Entity\ContentEntityBase->getTranslatedField().

## [2.0.1] - 2023-07-10

### Added

- If the Image field is in multiply bundles then created Media field will be in
the same bundles as the Image field.
- It's possible to choose an existing Media field. In this case a new Media
field won't be created. Images will be added to the existing Media field.
